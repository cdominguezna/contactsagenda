import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import {ContactsServiceService} from "../contacts-service.service";

@Component({
  selector: 'contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.scss']
})
export class ContactAddComponent implements OnInit {
  // Variables
  contactForm:FormGroup;
  constructor(private contactService:ContactsServiceService,
              private formBuilder: FormBuilder) {

    // S'inicialitza els formularis
    this.contactForm = this.formBuilder.group({
      firstName: new FormControl('', [ Validators.required  ]),
      lastName: new FormControl('', [ Validators.required   ]),
      phoneNumber: '',
      email:'',
      direccio:''
    });
  }

  ngOnInit() {

  }

  onSubmit() {
    // Etapa 9 | En crear un nou contacte apareixerà un missatge informant-nos que 
    // el nou contacte s'ha guardat correctament.
    this.contactService.addNewContact(this.contactForm.value)
      .then(() => {
        alert('Se ha añadido el contacto correctamente');
        // Reset dels inputs del formulari desprès de fer l'inserció
        this.resetForm();
      })

  }

  // Reset dels imputs del formulari
  resetForm(){
    this.contactForm.reset(
      {
        firstName: '' ,
        lastName: '',
        phone: '',
        email:'',
        direccio:''
      }
    );
  }

}

