import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { ContactAddComponent } from './contact-add/contact-add.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactItemComponent } from './contact-list/contact-item/contact-item.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import {ContactsServiceService} from "./contacts-service.service";
import { ContactHomeComponent } from './contact-home/contact-home.component';

// Routing
import { APP_ROUTING } from './app.routes';

const appRoutes: Routes = [
  { path: 'edit/:id', component: ContactDetailComponent },
  { path: '', component: ContactHomeComponent} 
];

@NgModule({
  declarations: [
    AppComponent,
    ContactAddComponent,
    ContactListComponent,
    ContactItemComponent,
    ContactDetailComponent,
    ContactHomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    APP_ROUTING
  ],
  providers: [
    ContactsServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
