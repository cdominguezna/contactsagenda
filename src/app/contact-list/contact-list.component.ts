import { Component, OnInit } from '@angular/core';
import {ContactsServiceService} from "../contacts-service.service";
import {Contact} from "../Contact";

@Component({
  selector: 'contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {

  //Array privat d'objectes que representen els contactes de la llista.
  contactList: Array<Contact>;

  constructor(private contactService:ContactsServiceService) {
    // Aquí recuperem la llista de contactes amb : id, firstName i lastName.
    this.contactList = this.contactService.getContacts();
  }

  ngOnInit() {
  }

}
