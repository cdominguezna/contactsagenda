import {Component, Input, OnInit} from '@angular/core';
import {ContactsServiceService} from "../../contacts-service.service";
import {Contact} from "../../Contact";

@Component({
  selector: 'contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent implements OnInit {
  @Input() contact: Contact;
  constructor(private contactService:ContactsServiceService) {

  }

  ngOnInit() {
  }

  //Etapa 9 | En esborrar un nou contacte se'ns demanarà confirmar si volem 
  //eliminar definitivament aquest contacte.
  removeItem(id: number) {
    if (confirm('Esta seguro que quiere eliminar el contacto?')) {
     
      this.contactService.removeContactById(id)
        .then(() => {

          //Informa a l'usuari que l'acció d'esborrar el registre a sigut realitzada correctament
          console.log("Se ha eliminado el registro");
        })
    }
  }

}
