//Etapa 9 | Afgir nous detalls del contacte
export interface Contact {
  id: number;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: String;
  direccio: String;
}
