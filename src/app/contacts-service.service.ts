//Etapa 6 | Centralitzar les tasques que componen la contact list (service)

import { Injectable } from '@angular/core';
import {Contact} from "./Contact";


@Injectable()
export class ContactsServiceService {

  // VARIABLES
  // Contactlist inicial on tindrem el id, firstName, lastName, phoneNumber.
  //Etapa 9 | 
  
  private _contactListDefault: Array<Contact> = [
    { id: 0,
      firstName: "John",
      lastName: "Doe",
      phoneNumber: "555123456",
      email: "jd@gmail.com",
      direccio: "Av. Barcelona"
     
    }
  ];
  // Array per guardar els contactes
  private _contactList: Array<Contact> = [];
  
  //id del contacte
  private _id: number;

  
  // Llista de contactes
  CONTACT_LIST_CONTACTS_KEY = 'contactListContacts';
  // Es mostra l'últim id
  CONTACT_LIST_LAST_ID_KEY = 'contactListLastId';

  constructor() {
    //Recuperem si existeix els contactes.
    this.checkContactsData();
  }

  //Gets
  // Recuperar la lista de contactos
  getContacts() {
    return this._contactList;
  }

  getContactById( id: number ) {
    var promise = new Promise((resolve, reject) => {
      let i: number;
      for ( i = 0; i < this._contactList.length; i++) {
        if ( this._contactList[i].id === id ) {
          //Si existeix recuperem el contacte
          resolve( this._contactList[i] );
        }
      }
      resolve(null);
    });
    return promise;
  }

  // Aqui afegim un nou contacte
  addNewContact( contact:Contact ) {
    var promise = new Promise((resolve, reject) => {
      const newContact: Contact = {
        id : this._id,
        firstName : contact.firstName,
        lastName: contact.lastName,
        phoneNumber: contact.phoneNumber,
        email: contact.email,
        direccio: contact.direccio
      };
      //Guardem les dades a l'array
      this._contactList.push(newContact);
      //incrementem l'id a guardar desde la posició pasada
      this._id++;
      // Actualitzem
      this.updateContactsData();
      resolve();
    });
    return promise;
  }

  // Eliminar el contacte amb el id
  removeContactById( id:number ) {
    var promise = new Promise((resolve, reject) => {
      let i: number;
      for ( i = 0; i < this._contactList.length; i++) {
        if ( this._contactList[i].id === id ) {
          this._contactList.splice(i, 1);
          break;
        }
      }
      // Actualitzem
      this.updateContactsData();
      resolve();
    });
    return promise;
  }

  // Editem el contacte amb el id indicat
  editContactById( id: number, value:Contact) {
    var promise = new Promise((resolve, reject) => {
      let i: number;
      for ( i = 0; i < this._contactList.length; i++) {
        if ( this._contactList[i].id === id ) {
          this._contactList[i].firstName = value.firstName;
          this._contactList[i].lastName = value.lastName;
          this._contactList[i].phoneNumber = value.phoneNumber;
          this._contactList[i].email = value.email;
          this._contactList[i].direccio = value.direccio;

          break;
        }
      }
      //Actualitzem
      this.updateContactsData();
      resolve();
      });
    return promise;
  }

  //ETAPA 8 | Possibilitat de guardar en local l'agenda de contactes.

  checkContactsData() {
    let storedData: any;
    storedData = store.get(this.CONTACT_LIST_CONTACTS_KEY);

    if ( storedData ) {
      this._contactList = storedData;
      this._id =  store.get(this.CONTACT_LIST_LAST_ID_KEY);
    }
    else {
      this._contactList = this._contactListDefault;
      this._id = this._contactListDefault.length;
      this.updateContactsData();
    }
  }
  updateContactsData() {
    store.set(this.CONTACT_LIST_CONTACTS_KEY, this._contactList);
    store.set(this.CONTACT_LIST_LAST_ID_KEY, this._id);
  }


}
