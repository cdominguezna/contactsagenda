import { Component, OnInit, OnDestroy} from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import {ContactsServiceService} from "../contacts-service.service";
import {Contact} from "../Contact";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
  selector: 'contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.scss']
})
export class ContactDetailComponent implements OnInit {
  // Variables
  editContactForm:FormGroup;
  id:number;
  private _subscription:any;
  private _initContact:Contact;

  constructor(private contactService:ContactsServiceService,
              private activatedRoute: ActivatedRoute,
              private formBuilder: FormBuilder,
              private router: Router) {

    // Inicialització del formulari
    this.editContactForm = this.formBuilder.group({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      phoneNumber: '',
      email:'',
      direccio:''
    });

  }

  ngOnInit() {
    this._subscription = this.activatedRoute.params.subscribe(params => {
      //Recuperem la id del contacte
      this.id = +params['id']; 
      //Aqui es recuperen les dades del contacte
      this.getContactData();
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }


  getContactData(){
    this.contactService.getContactById(this.id)
      .then((contact:Contact)=>{
        //Es guarden les dades recollides amb l'id
        this._initContact = contact;
       //Aqui es pasen al formulari
        this.initForm();
      });
  }

  initForm(){
    this.editContactForm = this.formBuilder.group(
      {
        firstName:   this._initContact.firstName,
        lastName:    this._initContact.lastName,
        phoneNumber: this._initContact.phoneNumber,
        email: this._initContact.email,
        direccio : this._initContact.direccio
      }
    );
  }

  resetForm(){
    this.initForm();
  }

  onSubmit() {
    
    this.contactService.editContactById(this.id, this.editContactForm.value)
      .then(() => {
        // Indiquem que s'ha modificat el registre
        alert('Se ha modificado el registro');
        // Reset del formulari
        this.resetForm();
        // Tornem
        this.router.navigate(['']);
      })

  }

  removeContact() {
    if (confirm('Estas seguro que quiere eliminar el registo ?')) {
      this.contactService.removeContactById(this.id)
        .then(() => {
          // Si s'ha realitzat correctament
          console.log("Se ha eliminado el registro");
          // Tornem a la pantalla home
          this.router.navigate(['']);
        })
    }
  }

}
