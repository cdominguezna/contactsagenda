//Etapa 6 | Creació del sistema de navegació (Route)

// Components
import { ContactHomeComponent } from './contact-home/contact-home.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';

// Routing
import {RouterModule, Routes} from "@angular/router";

const APP_ROUTES: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ContactHomeComponent },
  { path: 'edit/:id', component: ContactDetailComponent }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
